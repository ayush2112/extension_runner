const express = require('express');
const formidable = require('formidable');

const { unzip, moduleLoad } = require('@/utils');

const router = express.Router();

const UPLOAD_DIR = `${__dirname}/../extension_uploads`;
const UNZIP_PATH = `${__dirname}/../remote_mods`;

router.post('/', async (req, res) => {
  try {
    const form = formidable({
      multiples: true,
      uploadDir: UPLOAD_DIR,
      keepExtensions: true,
      maxFileSize: 5 * 1024 * 1024,
      filename: (name, ext, part, form) => `${name}${ext}`,
    });
    form.parse(req, async (err, fields, files) => {
      if (err) {
        next(err);
        return;
      }
      try {
        const moduleName = await unzip(files.file.filepath, UNZIP_PATH);
        if (!moduleName) {
          res.sendStatus(500);
        }
        await moduleLoad(moduleName);
        res.sendStatus(200);
      } catch (err) {
        console.log(err);
        res.sendStatus(500);
      }
    });
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
  }
});


module.exports = router;