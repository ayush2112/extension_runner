const express = require('express');
const router = express.Router();

router.get('/health', (req, res) => {
  res.status(200).send({message: 'Service is running'});
});
router.use('/extension/upload', require('./loadExtensions'));
router.use('/extension/', require('./runExtensions'));

// const router = function(app) {
//   this.app = app;
// }

// router.prototype.initRoutes = function() {
//   const app = this.app;

//   app.use('/', (req, res) => {
//     res.sendStatus(200);
//   })
//   app.use('/upload', require('./loadExtensions'));
//   app.use('/:service/:function', require('./runExtensions'));
// }
module.exports = router;