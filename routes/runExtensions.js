const express = require('express');
const router = express.Router();

const moduleHandler = require('../resources/modules');

router.post('/:service/:method', async (req, res) => {
  try {
    const { service, method } = req.params;
  
    const extensionExists = moduleHandler.serviceExists(service);
    if (!extensionExists) {
      res.sendStatus(404);
      return;
    }
  
    const extension = moduleHandler.getExtensionByName(service);
    const func = extension.functions[method];
  
    if (!func) {
      res.sendStatus(404);
      return;
    }
  
    const params = {};
  
    ['body', 'params', 'query'].map((reqComp) => {
      func.expects?.[reqComp]?.map?.((bodyParam) => {
        if (req?.[reqComp]?.hasOwnProperty(bodyParam)) {
          params[bodyParam] = req[reqComp][bodyParam];
        }
      })
    });
  
    let response = await extension.$ref[func.name](params);
    let status = 200;
    if (response.hasOwnProperty('status') && typeof response.status === 'number') {
      status = response.status;
    }
    if (response.hasOwnProperty('status') && response.hasOwnProperty('data')) {
      response = response.data;
    }
    res.status(status).send(response);
  } catch (err) {
    res.status(500).send('Something went wrong');
  }
});

router.get('/:service?/:method?', async (req, res) => {
  const {service, method} = req.params;
  let response;
  
  if (!service) {
    response = moduleHandler.getMap();
  } else if(service && !method) {
    response = moduleHandler.getExtensionByName(service);
  } else if(service && method) {
    const ext = moduleHandler.getExtensionByName(service);
    response = ext?.functions?.[method];
  } else {
    res.sendStatus(400);
    return;
  }
  if (!response) {
    res.status(404).send('Extension details not found');
  } else {
    res.status(200).send(response);
  }
});

router.delete('/:service', async (req, res) => {
  const {service} = req.params;
  let response;
  
  moduleHandler.deleteService(service);
  
  res.status(200).send(response);
});

module.exports = router;