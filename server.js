require('module-alias/register');

const express = require('express');
const cors = require('cors');
const app = express();

const routes = require('@/routes');

app.use(cors());

app.use(express.json({limit: '1mb'}));
app.use(express.urlencoded({ limit: '1mb', extended: false }));

// const router = new routes(app);
// router.initRoutes();
app.use('/', routes);

app.use((err, req, res, next) => {
  /// set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

const port = 3030;
app.set('port', port);

app.listen(port, () => {
  console.log(`Listening on ${port}`);
});