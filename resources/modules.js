class ModuleHandler {
  
  constructor() {
    this.moduleMap = {};
  }

  updateService(key, val) {
    this.moduleMap[key] = val;
    return this.moduleMap;
  }

  addService(serviceName, map) {
    this.moduleMap[serviceName] = {
      $ref: map.$ref,
      functions: map.functions,
    };
  }

  deleteService(service) {
    delete this.moduleMap[service];
  }

  getMap() {
    return this.moduleMap;
  }

  getExtensionByName(service) {
    return this.moduleMap[service] ?? null;
  }

  serviceExists(name) {
    return !!this.moduleMap[name];
  }
}

module.exports = new ModuleHandler();