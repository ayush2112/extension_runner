#!/bin/bash

DATE=`date +%Y.%m.%d.%H.%M`

echo 'script'

NPMLIBS=''
i=1;
j=$#;
while [ $i -le $j ] 
do
    NPMLIBS+=$1;
    NPMLIBS+=' ';
    i=$((i + 1));
    shift 1;
done

if ["$NPMLIBS" -eq '']
then
    echo "No libs provided";
    exit 1;
fi

echo $NPMLIBS
npm install $NPMLIBS
echo 'Installed'