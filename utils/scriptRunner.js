const { exec } = require('child_process');

const { locationExists } = require('./utils');

const SCRIPT_DIR = `./scripts`;

const runScript = async (scriptName, params, options) => new Promise(async (res, rej) => {
  try {
    if (options.runWithParams && !params.filter(l => !!l).length) {
      res();
      return;
    }
    const scriptLoc = `${SCRIPT_DIR}/${scriptName}`;
    // const scriptLoc = `${scriptName}`;
    const scriptExists = await locationExists([scriptLoc]);
    if (scriptExists) {
      const libs = params.join(' ');
      const command = `bash ${scriptLoc} ${libs}`;
      exec(command, (error, stdout, stderr) => {
        console.log('stdout');
        console.log(stdout);
        console.log(stderr);
        if (error !== null) {
          console.log(`exec error: ${error}`);
          rej();
        } else {
          res();
        }
      });
    } else {
      console.log('Script does not exist');
      rej('Script does not exist');
    }
  } catch (err) {
    console.log(err);
    rej();
  }
});

module.exports = runScript;