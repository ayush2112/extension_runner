const fs = require('fs');
const util = require('util');
const stat = util.promisify(fs.stat);

exports.locationExists = async(locations) => {
  try {
    const checks = locations.filter(l => !!l).map(l => stat(l));
    await Promise.all(checks);
    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
}