const fs = require('fs');

const runScript = require('./scriptRunner');
const { locationExists } = require('./utils');

const moduleHandler = require('@/resources/modules');

const MODULE_DIR = `${__dirname}/../remote_mods`;

const initialiseModule = async (initAdr, indexAdr) => {
  try {
    const ref = require(initAdr);
    // stages: NPM, service name, function names
    if (ref.NPM) {
      console.log('Scripting');
      await runScript('npm.sh', ref.NPM, { runWithParams: true });
      console.log('Scripted');
    }
    // module_alias?
    const serviceName = ref.serviceName;
    const callMap = ref.functions;
    console.log(callMap);

    return {
      serviceName,
      $ref: require(indexAdr),
      functions: callMap,
    }
  } catch (err) {
    console.error(err);
    return;
  }
}

const loadModule = async (name) => {
  const moduleAddress = `${MODULE_DIR}/${name}`;
  const moduleIndex = `${MODULE_DIR}/${name}/index.js`;
  const moduleInit = `${MODULE_DIR}/${name}/init.js`;
  
  const exists = await locationExists([moduleAddress, moduleIndex, moduleInit]);
  if (!exists) {
    throw new Error('Module files not loaded'); // the directory or one of the files doesn't exist
  }

  const moduleObject = await initialiseModule(moduleInit, moduleIndex);
  if (!moduleObject) {
    throw new Error('Module files not loaded due to Error');
  }

  if (moduleHandler.serviceExists(moduleObject.serviceName)) {
    throw new Error('Duplicate extension detected');
  } else {
    moduleHandler.addService(moduleObject.serviceName, moduleObject);
  }
}

module.exports = loadModule;