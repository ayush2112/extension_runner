const yauzl = require('yauzl');
var path = require('path');
var fs = require('fs');
var Transform = require('stream').Transform;

const mkdirp = (dir, cb) => {
  if (dir === ".") return cb();
  fs.stat(dir, function(err) {
    if (err == null) return cb(); // already exists

    var parent = path.dirname(dir);
    mkdirp(parent, function() {
      process.stdout.write(dir.replace(/\/$/, "") + "/\n");
      fs.mkdir(dir, cb);
    });
  });
}

const handleZipFile = (zipFilePath, unzipPath = '') => {
  return new Promise((res, rej) => {
    try {
      yauzl.open(zipFilePath, {lazyEntries: true}, (err, zipfile) => {
        if (err) throw err;
        var handleCount = 0;
        function incrementHandleCount() {
          handleCount++;
        }
        function decrementHandleCount() {
          handleCount--;
          if (handleCount === 0) {
            console.log("all input and output handles closed");
            res();
          }
        }
      
        incrementHandleCount();
        zipfile.on("close", function() {
          decrementHandleCount();
        });
      
        zipfile.readEntry();
        const uzPath = unzipPath.endsWith('/') ? unzipPath : unzipPath + '/';
        zipfile.on("entry", function(entry) {
          entry.fileName = uzPath + entry.fileName;
          if (/\/$/.test(entry.fileName)) {
            mkdirp(entry.fileName, function() {
              if (err) throw err;
              zipfile.readEntry();
            });
          } else {
            mkdirp(path.dirname(entry.fileName), function() {
              zipfile.openReadStream(entry, function(err, readStream) {
                if (err) throw err;
                // report progress through large files
                var byteCount = 0;
                var totalBytes = entry.uncompressedSize;
                var lastReportedString = byteCount + "/" + totalBytes + "  0%";
                process.stdout.write(entry.fileName + "..." + lastReportedString);
                function reportString(msg) {
                  var clearString = "";
                  for (var i = 0; i < lastReportedString.length; i++) {
                    clearString += "\b";
                    if (i >= msg.length) {
                      clearString += " \b";
                    }
                  }
                  process.stdout.write(clearString + msg);
                  lastReportedString = msg;
                }
                // report progress at 60Hz
                var progressInterval = setInterval(function() {
                  reportString(byteCount + "/" + totalBytes + "  " + ((byteCount / totalBytes * 100) | 0) + "%");
                }, 1000 / 60);
                var filter = new Transform();
                filter._transform = function(chunk, encoding, cb) {
                  byteCount += chunk.length;
                  cb(null, chunk);
                };
                filter._flush = function(cb) {
                  clearInterval(progressInterval);
                  reportString("");
                  // delete the "..."
                  process.stdout.write("\b \b\b \b\b \b\n");
                  cb();
                  zipfile.readEntry();
                };
      
                // pump file contents
                var writeStream = fs.createWriteStream(entry.fileName);
                incrementHandleCount();
                writeStream.on("close", decrementHandleCount);
                readStream.pipe(filter).pipe(writeStream);
              });
            });
          }
        });
      });
    } catch (err) {
      console.log(err);
      rej();
    }
  });
}

const unzipModule = async (zipFilePath, unzipPath) => {
  // url -> download to remote_mods, filename
  try {
    await handleZipFile(zipFilePath, unzipPath ?? ''); // remote_mods + filename
    const filename = (zipFilePath.split('/')[zipFilePath.split('/').length - 1]).replace('.zip', '');
    return filename;
  } catch (err) {
    console.log(err);
    return null;
  }
}

module.exports = unzipModule;